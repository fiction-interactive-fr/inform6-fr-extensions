!---------------------------------------------------------------------------
!              EffetsDeTexte
!
!        par   Hugo Labrande   ( pr�nom at hlabrande point fr)
!
!    Version : 20/08/2015
!
!  (Licence : Domaine public)
!
!---------------------------------------------------------------------------
!(You speak English? Go to the very bottom for the doc in English!)
!
!
!    Biblioth�que bi-plateforme pour rajouter plein d'effets de texte �
!   votre jeu. Rajoute ~4K � votre fichier final. Inclut les effets :
!     - police normale (proportionnelle) ou non proportionnelle
!     - gras, italique, gras+italique
!     - couleurs invers�es (�crire en blanc sur noir quand le reste est
!       noir sur blanc)
!     - en couleurs
!     - et toutes les combinaisons possibles
!    ex: Afficher("Mon texte", GRAS|ITALIQUE|VERT);
!        Afficher("Mon texte2", INVERSE|NON_PROP|BLEU+JAUNE);
!
!   mais aussi :
!     - tout en majuscule, ou en alternant majuscules et minuscules,
!       ou m�me en d�finissant une probabilit� qu'une lettre passe en
!       majuscule
!    ex: Majuscules("Mon texte");
!        MajusculesAlternees("Mon texte");
!        MajusculesAlternees("Mon deuxi�me texte", 3);  ! 1 sur 3 est majuscule
!        MajusculesAleatoires("Mon texte al�atoire", 40); !40% de chance de majuscule
!
!     - centr� (avec un des styles ci-dessus ; on peut m�me varier les
!       styles au sein du texte)
!    ex: DebutCentre(20);         ! 10% de marge � gauche et � droite
!        AffichageCentre("Mon texte");
!        AffichageCentre(" la suite en bleu^et un peu plus", BLEU);
!        FinCentre();
!
!     - texte affich� apr�s un certain temps d'attente ou apr�s que le
!       joueur ait appuy� sur une touche
!    ex: ApresAttente("BOUH!", 10);     ! 10 dixi�mes de seconde
!
!     - texte affich� apr�s un certain temps d'attente incompressible (ou
!       presque)
!    ex: ApresAttenteIncompressible("BOUH", 10);
!
!     - texte affich� progressivement (effet 'machine � �crire')
!    ex: Progressif("Leeeeentemeeeent", 5);  ! 1 = lent, 10 = rapide, -1 = tr�s lent
!
!     - texte affich� une lettre � la fois � chaque fois que le joueur
!       appuie sur une touche
!    ex: LettreParLettre("embrasser juliette");
!       (effet invent� � ma connaissance par Adam Cadre dans Shrapnel)
!
!     - karaok� (non implant� pour l'instant !)
!
!    Si vous devez combiner certains de ces effets, il faudra sans doute
!   que vous fassiez vos propres routines ; mais nul doute que 90% de vos
!   besoins seront couverts par cette extension.
!    Si vous avez des id�es, des commentaires ou des critiques sur cette
!   extension, envoyez-moi un mail !
!    Si cette extension ne vous convient pas, vous pouvez regarder les
!   extensions suivantes: "utility.h" par L. Ross Raszewski, "orcenter.h"
!   par Jim Fisher, "OKBStyle.h" par Brendan Barnwell, "text_functions.h"
!   par Patrick Kellum (dont le code de certaines fonctions ici s'inspire),
!   "Markup" par Simon Baldwin, et "printslow.h" par David Cornelson.
!
!
!---------------------------------------------------------------------------
!
!   Utilisation : rajoutez
!          Include "EffetsDeTexte";
!    � la ligne juste apr�s Include "FrenchG" dans votre code.
!
!   ATTENTION :
!    - Afficher() ne marche pas bien en Glulx. En effet, Glulx traite les styles
!   de fa�on tr�s diff�rente de la Z-machine, et de fa�on beaucoup plus limit�e
!   d'ailleurs : on ne peut pas d�finir des styles dynamiquement, et je ne veux pas
!   toucher aux styles r�serv�s pour l'auteur, alors on ne peut rien faire.
!    - AffichageCentre() fait du mieux qu'il peut, mais vu qu'on n'a pas acc�s
!   � la police de caract�res de l'interpr�teur l'effet sera imparfait - � moins
!   d'utiliser la police non-proportionnelle.
!
!    Certains effets complexes ont besoin d'un tableau pour faire leurs op�rations;
!   ce tableau est de taille 1000 par d�faut. Soit vous cassez votre texte en
!   plusieurs morceaux, soit vous augmentez la taille du tableau (et donc la
!   taille de votre jeu).
!
!---------------------------------------------------------------------------

System_file;

Constant COLOR;   ! for Inform 6/12



! Quand on utilise print_to_array, '^' est remplac� par un caract�re de retour � la ligne
!       mais Zcode utilise 'carriage return' ('\r'), la norme sur des vieux micros (ZX, C64, etc) et Mac avant OSX
!       et Glulx utilise 'line feed' ('\n'), comme Unix.
!   Il y a plein de normes et de conventions diff�rentes: Windows utilise '\r\n', ce qui fait
!       que des fichiers Linux apparaissent comme une seule ligne sous Windows parfois
!   Moralit� : on tombe en plein dans un d�bat de standardisation et de geek
#Ifdef TARGET_ZCODE;
Constant NEW_LINE_CHAR = 13;     ! '\r'
#Ifnot; ! TARGET_GLULX
Constant NEW_LINE_CHAR = 10;     ! '\n'
#Endif;





!  C'est peut-�tre impossible en inform, mais �a serait bien qu'on ne soit
! pas contraint par la taille du tableau : si on pouvait mettre les 200 premi�res
! lettres du string dans un tableau, les traiter, puis les 200 suivantes, les
! traiter, etc, �a serait bien.

! Le tableau d'appoint
Constant TAILLE_TABLEAU_APPOINT = 1000;
Array appoint buffer TAILLE_TABLEAU_APPOINT;
[ NettoyerAppoint i; for (i=0 : i<appoint-->0 : i++) appoint->(WORDSIZE+i) = 0; ];

! Les tableaux d'appoint pour le centrage
Constant TAILLE_TABLEAU_APPOINT_CENTRAGE = 400; ! pas plus de 400 caract�res par ligne, �a semble pas illogique
Array ligne_centree buffer TAILLE_TABLEAU_APPOINT_CENTRAGE;
Array styles_ligne_centree buffer TAILLE_TABLEAU_APPOINT_CENTRAGE;
[ NettoyerLigneCentree i;
  for (i=0 : i<ligne_centree-->0 : i++) {
	  ligne_centree->(WORDSIZE+i) = 0;
	  styles_ligne_centree->(WORDSIZE+i) = 0;
  }
];
Array appoint_centre_input buffer TAILLE_TABLEAU_APPOINT; ! le tableau qui r�ceptionne ce que dit l'auteur






!----------------------------------------------------------------------------
!                                STYLE DU TEXTE



!=====================================
!    Constantes

! Constantes de style : les m�mes que Inform
Constant ROMAN =        $$00000000;
Constant NORMAL =       $$00000000;
Constant INVERSE =		$$00000001;
Constant GRAS = 		$$00000010;
Constant ITALIQUE =		$$00000100;
Constant NONPROP =      $$00001000;
Constant N_P =			$$00001000;
Constant NON_PROP =     $$00001000;

! Constantes de couleurs: suivent les r�gles d'addition de RGB (sauf noir)
Constant NOIR =         $$00010000;
Constant ROUGE =        $$00100000;
Constant VERT =         $$01000000;
Constant BLEU =         $$10000000;
Constant JAUNE =        $$01100000;
Constant CYAN =         $$11000000;
Constant VIOLET =       $$10100000;
Constant MAGENTA =      $$10100000;
Constant BLANC =        $$11100000;

!

!=====================================
! Est-ce que l'interpr�teur prend en charge...

[ IsColourSupported ;
#Ifdef TARGET_ZCODE;
  return ((0->1) & 1 ~= 0);
#Ifnot; ! TARGET_GLULX
  return true;              ! Techniquement, la couleur est toujours support�e ; en pratique,
                            !   on ne peut pas s'en servir
#Endif;
];

[ IsTimeSupported ;
#Ifdef TARGET_ZCODE;
  return ((0->1) & 128 ~= 0);
#Ifnot; ! TARGET_GLULX
  return glk_gestalt(gestalt_Timer, 0);
#Endif;
];


!=====================================
!    Affichage / Afficher (routine g�n�rique)
!   Prend deux arguments, le texte et le style

[ SetStyle style sty16 ;

  ! On commence par partir du default (pour que =0 soit bien en roman)
  #Ifdef TARGET_ZCODE;
    @set_text_style 0;
    if (IsColourSupported() == true) {
      SetColour(1, 1);
    }
  #Ifnot; ! TARGET_GLULX
    glk_set_style(style_Normal);
  #Endif;

  ! Non-couleur
  sty16 = style % 16;
  #Ifdef TARGET_ZCODE;
  if (standard_interpreter > $100) {
	! standard 1.1 et plus implantent @set_text_style = 5
	! mais devrait-on le faire nous-m�me pour des raisons de rapidit� ? pas s�r
    @set_text_style sty16;
  }
  else {
    ! cf http://www.wolldingwacht.de/if/z-spec10.pdf p162:
    if (sty16 & $$1000) {@set_text_style 8;}
    if (sty16 & $$0100) {@set_text_style 4;}
    if (sty16 & $$0010) {@set_text_style 2;}
    if (sty16 & $$0001) {@set_text_style 1;}
    ! Ce code en assembleur z-machine est peut-�tre plus rapide (m�me si l� on chipote) ; il devrait marcher, mais non. Si quelqu'un a une id�e...
        !._fixedwidth;
	    !@loadb sty16 3 ->sp;
	    !@jz sp ?_bold;
	    !@set_text_style 8;
	    !._bold;
	    !@loadb sty16 2 ->sp;
	    !@jz sp ?_italics;
	    !@set_text_style 4;
	    !._italics;
	    !@loadb sty16 1 ->sp;
	    !@jz sp ?_reverse;
	    !@set_text_style 2;
	    !._reverse;
	    !@loadb sty16 0 ->sp;
	    !@jz sp ?_roman;
	    !@set_text_style 1;
	    !._roman;
	! Test� sur wzip21, terp antique (1994), qui apparemment ne comprend pas que @set_text_style 8 c'est non-prop (mais fait du non-prop avec style fixed). Croisons les doigts quoi
  }
  #Ifnot; ! TARGET_GLULX

  ! Les interpr�teurs Glulx ne sont jamais d'accord. Voil� leurs valeurs par d�faut:

  !					Gargoyle		WinGit			WinGlulxe		Quixe
  !	Emphasized		italics			Bold			Bold			Italics
  !	Preformatted	Fixed-width		Fixed-width		Fixed-width		Fixed-width
  !	Header			Bold			Bold (larger)	Bold (larger?)	Bold (larger)
  !	Subheader		Bold			Bold (larger?)	Bold			Bold (larger?)
  !	Alert			Bold + italics	Bold (larger?)	Bold			Bold
  !	Note			Italics			Italics			Italics			Italics
  !	BlockQuote		Normal			Normal			Normal			Normal fond jaune
  !	Input			Green			Bold			Bold			Dark red

    ! Tout le monde s'accorde sur Note = Italics et Preformatted = Fixed-width
    if (sty16 & $$1000) {glk_set_style(style_Preformatted);}
    if (sty16 & $$0100) {glk_set_style(style_Note);}
    ! Subheader est en gras, mais des fois plus gros ; Alert est gras, mais italique sur Gargoyle.
    ! On choisit subheader, parce que c'est pas plus gros de beaucoup quand �a l'est
    if (sty16 & $$0010) {glk_set_style(style_Subheader);}
    ! Pour le reverse, BlockQuote est le style qui s'approche le plus
    !    (rappelons-nous que le reverse c'est surtout utilis� pour faire des quotes en Z-Machine)
    ! Ca fera un joli truc pour Quixe et rien pour le reste, pas grave
    if (sty16 & $$0001) {glk_set_style(style_BlockQuote);}
  #Endif;

  ! La couleur
  if (IsColourSupported() == true) {
  #Ifdef TARGET_ZCODE;
	clr_on = 1;
	if (style >= 16) {
      sty16 = (style-sty16)/32+ 2;
      SetColour(sty16, 1);
    }
  #Ifnot; ! TARGET_GLULX
    ! Ne rien faire: on ne peut pas afficher des couleurs sans toucher aux styles sp�ciaux
  #Endif;
  }
];

[ Affichage str style;
  ! Attention ! en glulx, �a ne marchera pas super bien ; �a d�pend des styles et des interpr�teurs.
  !   On peut pas se permettre de toucher aux styles de l'utilisateur non plus. Alors advienne que pourra

  SetStyle(style);
  print (string) str;
  SetStyle(0);

  return true;
];


[ Afficher string style ;
  Affichage(string, style);
];

[ AfficherTexte string style;
  Affichage(string, style);
];



!=====================================
!        Majuscules
!   pour mettre votre texte tout en majuscules, ou partiellement !
!   Longueur limit�e par la taille du tableau d'appoint


[ Majuscules str i c;
  NettoyerAppoint();
  str.print_to_array(appoint, TAILLE_TABLEAU_APPOINT);
  for (i = 0: i<appoint-->0: i++) {
    c = appoint->(WORDSIZE+i);
    c = UpperCase(c);
    print (char) c;
  }
  return true;
];

[ Capitales str ;
  return Majuscules(str);
];
[ CapsLock str ;
  return Majuscules(str);
];
[ VerrouillageMajuscules str ;
  return Majuscules(str);
];


! Alterner les majuscules et les minuscules, cOmMe fOnT LeS aDoS
!       Deuxi�me argument : optionnel, pour sp�cifier "une lettre toutes les n lettres est majuscule"
[ AlternerMajuscules str n i j c;
  NettoyerAppoint();
  str.print_to_array(appoint, TAILLE_TABLEAU_APPOINT);
  if (n<=0) { n=2; }
  j=1;
  for (i = 0: i<appoint-->0: i++) {
    c = appoint->(WORDSIZE+i);
	if (j == n) { print (char) UpperCase(c); j=1; }
	else { print (char) c; j++; }
  }
  return true;
];

[ MajusculesAlternees str n;
  return AlternerMajuscules(str, n);
];

! Majuscules al�atoires : certaines lettres passent en majuscule avec probabilit� p (exprim� en %)
[ MajusculesAleatoires str p i c;
  NettoyerAppoint();
  str.print_to_array(appoint, TAILLE_TABLEAU_APPOINT);
  if (p<0 || p>100) { p=50;}
  for (i = 0: i<appoint-->0: i++) {
    c = appoint->(WORDSIZE+i);
	if (random(100) <= p) { print (char) UpperCase(c); }
	else { print (char) c; }
  }
  return true;
];



!=====================================
!    Centr� (texte centr�)

! Lancez "DebutCentre(pourcent)", l'argument �tant le pourcentage des marges (20% = 10% � gauche et 10% � droite)
! Ensuite �crivez ce que vous voulez avec des "AfficherCentre("truc", GRAS)"
!  Astuce : si vous voulez rajouter un peu plus d'espaces pour que �a aie l'air plus centr�, modifiez la variable
!    globale ext_extra_spaces � votre convenance ; par exemple:
!      DebutCentre(20);
!      AfficherCentre("Premi�re ligne du texte");
!      ext_extra_spaces = 5;
!      AfficherCentre("Cette ligne est plus longue, il faut rajouter des espaces pour la centrer");
!      ext_extra_spaces = 0;
!      FinCentre();
!    Gardez cependant � l'esprit que ce qui est centr� pour votre police ne le sera pas forc�ment pour le joueur ;
!    l'effet de centrage est condamn� � �tre imparfait tant que le joueur peut changer de police.
! Essayez de ne rien mettre entre, ou alors finissez toujours par ^, sinon y'a des trucs qui vont pas s'afficher
! Quand vous avez fini, lancez "FinCentre()"

Global ext_centre = 0;
Global ext_centre_width = 0;
Global ext_centre_dejaimprimes = 0;
Global ext_extra_spaces = 0;

[ AfficheLigneCentree len current_style i;
  SetStyle(N_P);
  Print__Spaces( (ScreenWidth()-len)/2 + ext_extra_spaces);
  current_style = N_P;
  for (i=0: i<len: i++) {
	  if (current_style ~= styles_ligne_centree->(WORDSIZE+i)){
		  current_style = styles_ligne_centree->(WORDSIZE+i);
		  SetStyle(current_style);
	  }
	  print (char) ligne_centree->(WORDSIZE+i);
  }
  print "^";
  NettoyerLigneCentree();
  SetStyle(0);
];

[ DebutCentre pourcent w;
  w = ScreenWidth();
  if (pourcent < 0 || pourcent > 100) {ext_centre_width = w;}
  else {ext_centre_width = (w*(100-pourcent))/100;}
  NettoyerLigneCentree();
  ext_centre = 1;
  ext_centre_dejaimprimes = 0;
];

[ FinCentre ;
  AfficheLigneCentree(ext_centre_dejaimprimes);
  ext_centre = 0; ext_centre_width = 0; ext_centre_dejaimprimes = 0;
];

[ AfficherCentre string style rem i c pos;
  if (ext_centre) {
	  string.print_to_array(appoint_centre_input, TAILLE_TABLEAU_APPOINT);
	  rem = appoint_centre_input-->0;
	  pos = 0;
	  i = ext_centre_dejaimprimes;
	  while (rem ~=0) {
		  ! Copie un maximum de caract�res, ie tant que la ligne n'est pas pleine ou qu'on n'a pas un "^"
		  c = appoint_centre_input->(WORDSIZE+pos); pos++; rem--;
          if (c == NEW_LINE_CHAR || i == ext_centre_width) {
			  ! Affiche la ligne
			  AfficheLigneCentree(i); i = 0;
			  ! si c'est pas un "^", ajoute le caract�re
			  if (c ~= NEW_LINE_CHAR) {
				  ligne_centree->(WORDSIZE+i) = c;
				  styles_ligne_centree->(WORDSIZE+i) = style;
				  i = 1;
			  }
		  }
		  else {
			  ! Ajoute le caract�re avec le bon style
			  ligne_centree->(WORDSIZE+i) = c;
			  styles_ligne_centree->(WORDSIZE+i) = style;
			  i++;
		  }

	  }
	  ext_centre_dejaimprimes = i;
  }
  else { print string; }
];

[ AffichageCentre string style;
  AfficherCentre(string, style);
];

[ AfficherTexteCentre string style;
  AfficherCentre(string, style);
];

[ AffichageTexteCentre string style;
  AfficherCentre(string, style);
];


!----------------------------------------------------------------------------
!                               EFFETS TEMPORELS

!=====================================
!    Apr�sAttente et Apr�sAttenteIncompressible
!   (afficher quelque chose apr�s un certain temps)

[ ApresAttente str tenths ;
  if (IsTimeSupported() == true) {
    KeyDelay(tenths);
  }
  ! Si l'interpr�teur ne prend pas en charge les effets temporels, on affiche
  !    juste le texte
  print (string) str;
  return true;
];

[ ApresAttenteIncompressible str tenths v;
  ! Une attente peut toujours �tre �court�e en appuyant sur une touche
  ! Au lieu d'une attente, on attend beaucoup de fois 1/10s, ce qui fait
  !   que quand la joueuse appuie sur une touche il n'y a qu'une seule attente
  !   �court�e. Bien s�r, si la joueuse est debout sur une touche, �a sera
  !   bien moins long (moiti� moins environ, selon votre interpr�teur),
  !   mais on ne peut rien y faire.
  ! Ce truc n'est pas de moi, mais est tir� de l'extension "Markup"
  if (IsTimeSupported() == true) {
    for (v=0: v<tenths: v++){
      KeyDelay(1);
    }
  }
  ! Si l'interpr�teur ne prend pas en charge les effets temporels, on affiche
  !    juste le texte
  print (string) str;
  return true;
];

[ AffichageApresAttente str tenths ;
  ApresAttente(str, tenths);
];
[ AfficherApresAttente str tenths ;
  ApresAttente(str, tenths);
];


!=====================================
!    Progressif ou MachineAEcrire(texte, vitesse)
!   (10 = rapide, 1 = lent, -1 et moins = super lent)


[ Progressif txt vitesse i c j packet delay;
  ! Note: je m'�tais pris la t�te pendant quelques heures pour essayer de voir si, en prenant
  !   en compte la diff�rence Glulx/Z-code, on pourrait pas faire des trucs chouettes (genre
  !   lisser plus l'affichage des lettres). Et bien non, en tout cas toutes les solutions que
  !   j'ai essay� ne marchent pas super bien (l'effet ne se voit pas � l'oeil nu et �a ralentit
  !   la vitesse maximale d'affichage parce qu'on fait des tests en plus). Et le but c'est
  !   d'avoir un truc coh�rent entre Z-code et Glulx (si vous recompilez en Glulx, vous n'aurez
  !   en principe rien � changer concernant la vitesse, l'effet sera le m�me).
  ! Alors tant pis - mais si vous avez des id�es, dites-moi !


  ! 0 = vitesse instantan�e, 1 = vitesse la plus lente, et ainsi de suite
  ! Formule : (n^2+1)/2 lettres par dixi�me de secondes (1, 2, 5, 8, 13, 18, 25, 32, 41, 50)
  ! -1, -2, ... : n^2/4+2 dixi�mes de secondes par lettre (2, 3, 4, 6, 8, 11, 14, 18, 22, 27)

  if (vitesse == 0 || IsTimeSupported() == false) {print (string) txt; return true;}
  NettoyerAppoint();
  txt.print_to_array(appoint, TAILLE_TABLEAU_APPOINT);
  if (vitesse < 0) {
	packet = 1; delay = (vitesse*vitesse)/4+2;
  } else {
    packet = (vitesse*vitesse+1)/2; delay = 1;
  }
  j = 0;
  for (i=0: i<appoint-->0: i++)
  {
	  c = appoint->(WORDSIZE+i);
	  j++; if (j==packet) {
		j=0; KeyDelay(delay);
	  }
	  print (char) c;
  }
  return true;
];

[ MachineAEcrire txt vitesse;
  Progressif(txt, vitesse);
];
[ AffichageProgressif txt vitesse;
  Progressif(txt, vitesse);
];
[ AfficherProgressivement txt vitesse;
  Progressif(txt, vitesse);
];
[ AfficherTexteProgressivement txt vitesse;
  Progressif(txt, vitesse);
];
[ PrintSlow txt vitesse;
  Progressif(txt, vitesse);
];


!=====================================
!    LettreParLettre ou ToucheParTouche

! A chaque fois que le joueur appuie sur une touche, une lettre de plus s'affiche
! Utile si vous voulez faire comme si le joueur avait le choix d'�crire sa
!   commande, mais en r�alit� non.
! On n'a pas besoin des effets temporels pour un tel effet

[ LettreParLettre str i;
  NettoyerAppoint();
  str.print_to_array(appoint, TAILLE_TABLEAU_APPOINT);
  for (i = 0: i<appoint-->0: i++) {
    KeyCharPrimitive();
    print (char) appoint->(WORDSIZE+i);
  }
  KeyCharPrimitive();         ! Pour simuler l'appui sur la touche "Entr�e"
];

[ ToucheParTouche str;
  LettreParLettre(str);
];
[ AfficherLettreParLettre str;
  LettreParLettre(str);
];
[ SimulerInput str;
  LettreParLettre(str);
];
[ AffichageLettreParLettre str;
  LettreParLettre(str);
];


!=====================================
!    Karaok�

! Dans une future release, on pourra afficher du texte comme si on �tait au karaok�
!    Mais il faut que je r�fl�chisse � la conception du syst�me et des conventions d'abord












!============================================================
!     ENGLISH SPEAKERS, OVER HERE!

!    Here's some documentation for you, as well as English names for the functions
!
!    This extension has all the text effects I could think of:
!      - bold, italics, fixed-width and reverse; colors; and any combination of this
!            ex: WriteText("my text", EXT_BOLD|ITALICS|GREEN);
!      - all caps, alternate caps and non-caps (every other letter or every n letter),
!        define a probability for a letter to be a capital
!            ex: AllCaps("My text");
!                AlternatingCaps("My text");
!                AlternatingCaps("My other text", 3);  ! 1 out of 3 is capital
!                RandomCaps("My random text", 40); !40% chance of capital
!      - centered text on multiple lines with multiple styles
!            ex: BeginCenteredText(20);      ! margin = 10% on left, 10% on right
!                CenteredText("My text :");
!                WriteCenteredText("My text in bold", BOLD);
!                CenteredText("+ My test in yellow^And some more next line", YELLOW);
!                EndCenteredText();
!        with a caveat: since no one knows which font is used by the interpreter,
!           the effect will be imperfect except on fixed-width fonts
!       - wait x tenths of seconds or a keystroke and display text
!            ex: AfterWaiting("My text", 10);
!       - wait x tenths of seconds regardless of keystrokes and display text
!            ex: AfterStrictWaiting("My text", 10);
!       - slow printing of text (1 = slow, 10 = fast, -1 = really slow)
!            ex: WriteTextSlowly("Slooooow teeeext", 5);
!       - letter by letter after keystrokes, i.e. one more letter is printed when
!         the player hits a key. Useful if you want to simulate a command line input!
!            ex: LetterByLetter("kiss romeo");
!       - karaok�-style display - not implemented yet!
!
!    And it's bi-platform!... Well, as much as I could: using coloured text in Glulx
!  is just not possible, because the way the styles are set up (no way to change
!  styles dynamically, and only 2 user-defined styles that I leave for authors), and
!  bold and italics might be displayed as something different in some interpreters.
!    And it detects whether the interpreter supports color/time effects - crucial,
!  since Parchment doesn't support time, you wouldn't want your game to be unplayable
!  online because you wanted a typewriter effect.
!    And it comes at the low, low cost of 4K added to your game.

Constant EXT_REVERSE =		$$00000001;
Constant EXT_BOLD = 		$$00000010;
Constant ITALICS =		$$00000100;
Constant FIXEDWIDTH =   $$00001000;
Constant F_W =			$$00001000;
Constant FIXED_WIDTH =  $$00001000;
Constant BLACK =        $$00010000;
Constant RED =          $$00100000;
Constant GREEN =        $$01000000;
Constant BLUE =         $$10000000;
Constant YELLOW =       $$01100000;
Constant PURPLE =       $$10100000;
Constant WHITE =        $$11100000;


[ WriteText str style;
  return Affichage(str, style);
];

[ BeginCenteredText marg;
  return DebutCentre(marg);
];

[ EndCenteredText;
  return FinCentre();
];

[ WriteCenteredText str style;
  return AfficherCentre(str, style);
];

[ AfterWaiting str tenths ;
  return ApresAttente(str, tenths);
];

[ AfterStrictWaiting str tenths;
  return ApresAttenteIncompressible(str,tenths);
];

[ WriteTextSlowly str speed;
  return Progressif(str, speed);
];

[ LetterByLetter str;
  return LettreParLettre(str);
];

[ AllCaps str;
  return Majuscules(str);
];

[ Caps str;
  return Majuscules(str);
];

[ AlternatingCaps str n;
  return MajusculesAlternees(str, n);
];

[ RandomCaps str p;
  return MajusculesAleatoires(str,p);
];

